from django.contrib import admin
from django.urls import path
from account import views as login_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', login_views.home),
    path('login/', login_views.login),
    path('registration/', login_views.registration),
    path('logout/', login_views.logout),
    path('profile/', login_views.profile),
    path('password_change/', login_views.password_change),
]