from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.template.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from account.forms import UserCreationForm
from account.forms import LoginForm
from account.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash


def check_auth(request):
    if hasattr(request.user, 'email'):
        return 1
    else:
        return 0


def home(request):
    return redirect("/login/")


def login(request):
    args = {}
    args.update(csrf(request))
    args["form"] = LoginForm()

    if check_auth(request):
        return redirect("/profile/")

    elif request.POST:
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = auth.authenticate(username=email, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect("/profile/")
        else:
            args["login_error"] = "Пользователь не найден"
            return render_to_response("login.html", args)
    else:
        return render_to_response("login.html", args)


def logout(request):
    auth.logout(request)
    return redirect("/login/")


def registration(request):
    args = {}
    args.update(csrf(request))
    args["form"] = UserCreationForm()

    if check_auth(request):
        return redirect("/profile/")

    elif request.POST:
        user_form = UserCreationForm(request.POST)
        if user_form.is_valid():
            user_form.save()
            user = auth.authenticate(
                username=user_form.cleaned_data["email"],
                password=user_form.cleaned_data["password1"]
            )
            auth.login(request, user)
            return redirect("/profile/")
        else:
            args["form"] = user_form
    return render_to_response("registration.html", args)


def password_change(request):
    args = {}
    args.update(csrf(request))

    if not check_auth(request):
        return redirect("/login/")

    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('/profile/')
    else:
        form = PasswordChangeForm(request.user)
    args["form"] = form
    return render_to_response("password_change.html", args)


def profile(request):
    if not check_auth(request):
        return redirect("/login/")
    return render_to_response("profile.html", {"user": request.user})