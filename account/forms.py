from account.models import User
from django import forms
import datetime
import re
import traceback


widgets = {
    "email": forms.EmailInput(attrs={"class": "input"}),
    "year": forms.Select(attrs={"class": "input"}),
    "password": forms.PasswordInput(attrs={"class": "input"}),
}


def get_years():
    now_year = datetime.datetime.now().year
    years = reversed([(x, x) for x in range(now_year-100, now_year-10)])
    return years


def password_check(password1, password2):
    error = ""

    if password1 != password2:
        error = "Пароли не совпадают"
    elif len(password1) < 8:
        error = "Ваш пароль короче 8 символов"
    elif re.search(r"\d", password1) is None:
        error = "В вашем пароле нет цифр"
    elif re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password1) is not None:
        error = "Ваш пароль содержит запрещенные символы"

    if error:
        return {"password_ok": False, "error": error}
    else:
        return {"password_ok": True}


class UserCreationForm(forms.ModelForm):
    email = forms.EmailField(label="Почта", max_length=35, widget=widgets["email"])
    year = forms.ChoiceField(label="Год рождения", choices=get_years(), widget=widgets["year"])
    password1 = forms.CharField(label="Пароль", max_length=35, widget=widgets["password"])
    password2 = forms.CharField(label="Подтверждение пароля", max_length=35, widget=widgets["password"])

    class Meta:
        model = User
        fields = ["email"]

    def clean_email(self):
        email = self.cleaned_data.get("email")
        trigger = False
        try:
            User.objects.get(email=email)
            trigger = True
        except:
            traceback.print_exc()

        if trigger:
            raise forms.ValidationError("Такая почта уже зарегистрирована")
        else:
            return email

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        result = password_check(password1, password2)
        if result['password_ok'] is False:
            raise forms.ValidationError(result['error'])

        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.year = self.cleaned_data["year"]
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):
    email = forms.CharField(label='Почта', max_length=35, widget=widgets["email"])
    password = forms.CharField(label="Пароль", max_length=35, widget=widgets["password"])


class PasswordChangeForm(forms.Form):
    old_password = forms.CharField(label="Старый пароль", max_length=35, widget=widgets["password"])
    new_password1 = forms.CharField(label="Новый пароль", max_length=35, widget=widgets["password"])
    new_password2 = forms.CharField(label="Подтверждение пароля", max_length=35, widget=widgets["password"])

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')

        result = password_check(password1, password2)
        if result['password_ok'] is False:
            raise forms.ValidationError(result['error'])

        return password2

    def clean_old_password(self):
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError("Вы неправильно ввели старый пароль")

        return old_password

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()

        return self.user